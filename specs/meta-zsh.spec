%global app                     zsh
%global d_conf                  %{_sysconfdir}/%{app}

Name:                           meta-zsh
Version:                        1.0.1
Release:                        1%{?dist}
Summary:                        META-package for install and configure Zsh
License:                        GPLv3

Source10:                       %{app}rc

Requires:                       zsh

%description
META-package for install and configure Zsh.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{d_conf}/%{app}rc


%files
%config %{d_conf}/%{app}rc


%changelog
* Thu Oct 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-1
- NEW: v1.0.1.
- UPD: zshrc file.

* Thu Jul 18 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
